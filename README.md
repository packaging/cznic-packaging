# CZ.NIC packaging tracker 📦

This is a meta project for tracking **common CZ.NIC packaging** [issues],
features, and docs beyond the scope of individual projects.

## Please see [Issues][issues] 📑

[issues]: https://gitlab.nic.cz/packaging/cznic-packaging/-/issues
